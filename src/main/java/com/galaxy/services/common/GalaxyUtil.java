package com.galaxy.services.common;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Random;

import org.springframework.stereotype.Service;


@Service
public class GalaxyUtil {

	public String getSha256(String dataStr) throws Exception {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			return toHexString(md.digest(dataStr.getBytes(StandardCharsets.UTF_8)));
		} catch (Exception e) {
			throw e;
		}

	}

	public String toHexString(byte[] hash) {
		try {
			// Convert byte array into signum representation
			BigInteger number = new BigInteger(1, hash);

			// Convert message digest into hex value
			StringBuilder hexString = new StringBuilder(number.toString(16));

			// Pad with leading zeros
			while (hexString.length() < 32) {
				hexString.insert(0, '0');
			}

			return hexString.toString();
		} catch (Exception e) {
			throw e;
		}
	}


	
	public String getDefaultPassword() throws Exception {
		try {
			Random rnd = new Random();
			int number = rnd.nextInt(999999);
			return String.format("%06d", number);
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	   public String padLeftZeros(String inputString, int length) {
	        if (inputString.length() >= length) {
	            return inputString;
	        }
	        StringBuilder sb = new StringBuilder();
	        while (sb.length() < length - inputString.length()) {
	            sb.append('0');
	        }
	        sb.append(inputString);

	        return sb.toString();
	    }

}

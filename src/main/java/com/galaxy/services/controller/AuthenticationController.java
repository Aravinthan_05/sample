package com.galaxy.services.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.galaxy.services.entity.Customer;
import com.galaxy.services.entity.Employee;
import com.galaxy.services.model.request.EmployeeLogin;
import com.galaxy.services.model.response.GalaxyRes;
import com.galaxy.services.service.AuthenticationService;

@RestController
public class AuthenticationController {

	
	@Autowired
	AuthenticationService authenticationService;
	
	@PostMapping("/custlogin")
    public GalaxyRes login(@RequestBody EmployeeLogin employeeLogin){
		GalaxyRes gatGalaxyRes = new GalaxyRes();
     try
     {
    	 Customer customer = authenticationService.loginValidation(employeeLogin);
    	 if(customer != null)
    	 {
    	 gatGalaxyRes.adddata("customer", customer );
    	 gatGalaxyRes.setResCode("12"); 
		 gatGalaxyRes.setResDesc("Login was Sucessfully ");
    	 }else
    	 {
    		 gatGalaxyRes.setResCode("12"); 
    		 gatGalaxyRes.setResDesc("User was Not found ");
    	 }
     }catch (Exception e) {
    	 gatGalaxyRes.setResCode("12"); 
		 gatGalaxyRes.setResDesc(e.getMessage());
	}
     return gatGalaxyRes ;
    }
	
	
	
	@PostMapping("/forgetpassword")
    public GalaxyRes forgetPassword(@RequestBody EmployeeLogin employeeLogin){
		GalaxyRes gatGalaxyRes = new GalaxyRes();
     try
     {
    	 Customer customer = authenticationService.loginValidation(employeeLogin);
    	 if(customer != null)
    	 {
    	 gatGalaxyRes.adddata("customer", customer );
    	 gatGalaxyRes.setResCode("12"); 
		 gatGalaxyRes.setResDesc("Login was Sucessfully ");
    	 }else
    	 {
    		 gatGalaxyRes.setResCode("12"); 
    		 gatGalaxyRes.setResDesc("User was Not found ");
    	 }
     }catch (Exception e) {
    	 gatGalaxyRes.setResCode("12"); 
		 gatGalaxyRes.setResDesc(e.getMessage());
	}
     return gatGalaxyRes ;
    }
	
	
	
}

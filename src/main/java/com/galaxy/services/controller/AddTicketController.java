package com.galaxy.services.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.galaxy.services.entity.AddTicket;
import com.galaxy.services.entity.CategoryList;
import com.galaxy.services.service.AddTicketService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class AddTicketController {

    @Autowired
    private AddTicketService service;

    @PostMapping("/addTicket")
    public AddTicket addCustomer(@RequestBody AddTicket ticket){
        return service.saveTicket(ticket);
    }

    @GetMapping("/tickets")
    public List<AddTicket> findAllTickets(){return service.getTickets();}

    @GetMapping("/ticketById/{id}")
    public AddTicket findTicketById(@PathVariable int id){return service.getTicketById(id);}

    @DeleteMapping("/deleteTicket/{id]")
    public String deleteTicket(@PathVariable int id){
        return service.deleteTicket(id);
    }
    
    @GetMapping("/category")
    public List<CategoryList> findAllCategories(){return service.getCategories();}

}

package com.galaxy.services.controller;

import com.galaxy.services.entity.CategoryList;
import com.galaxy.services.service.AddCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
/* @CrossOrigin(origins = "*", maxAge = 3600) */
public class AddCategoryController {

    @Autowired
    private AddCategoryService service;

    @PostMapping("/addCategory")
    public CategoryList addCategory(@RequestBody CategoryList catg){
        return service.saveCategory(catg);
    }

    @GetMapping("/categories")
    public List<CategoryList> findAllCategories(){
    	System.out.println("TEst out Put Sample");
    	return service.getCategories();
    	}

    @GetMapping("/categoryById/{id}")
    public CategoryList findCategoryById(@PathVariable int id){return service.getCategoryById(id);}

    @DeleteMapping("/deleteCategory/{id]")
    public String deleteCategory(@PathVariable int id){
        return service.deleteCategory(id);
    }

}

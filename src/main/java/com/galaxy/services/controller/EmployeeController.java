package com.galaxy.services.controller;

import com.galaxy.services.entity.Employee;
import com.galaxy.services.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
//@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @GetMapping(value = "/test")
    public String testMethod(){
    	
        return "Testing";
    }


    @PostMapping("/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee){
        return service.saveEmployee(employee);
    }

    @PostMapping("/addEmployees")
    public List<Employee> addEmployees(@RequestBody List<Employee> employee){
        return service.saveEmployees(employee);
    }

    @GetMapping("/employees")
    public List<Employee> findAllEmployees(){return service.getEmployees();}

    @GetMapping("/employeeById/{id}")
    public Employee findEmployeeById(@PathVariable int id){return service.getEmployeeById(id);}

    @PutMapping("/updateEmployee")
    public Employee updateEmployee(@RequestBody Employee employee_customer){
        return service.updateEmployee(employee_customer);
    }
    
	

    @DeleteMapping("/deleteEmployee/{id]")
    public String deleteEmployee(@PathVariable int id){
        return service.deleteEmployee(id);
    }

}

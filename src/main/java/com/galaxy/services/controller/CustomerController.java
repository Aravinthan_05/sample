package com.galaxy.services.controller;

import com.galaxy.services.entity.Customer;
import com.galaxy.services.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class CustomerController {

    @Autowired
    private CustomerService service;

    @PostMapping("/addCustomer")
    public Customer addCustomer(@RequestBody Customer customer){
    	
        return service.saveCustomer(customer);
    }

    @PostMapping("/addCustomers")
    public List<Customer> addCustomers(@RequestBody List<Customer> customer){
        return service.saveCustomers(customer);
    }

    @GetMapping("/customers")
    public List<Customer> findAllCustomers(){return service.getCustomers();}

    @GetMapping("/customerById/{id}")
    public Customer findCustomerById(@PathVariable int id){return service.getCustomerById(id);}

    @PutMapping("/updateCustomer")
    public Customer updateCustomer(@RequestBody Customer employee_customer){
        return service.updateCustomer(employee_customer);
    }

    @DeleteMapping("/deleteCustomer/{id]")
    public String deleteCustomer(@PathVariable int id){
        return service.deleteCustomer(id);
    }

}

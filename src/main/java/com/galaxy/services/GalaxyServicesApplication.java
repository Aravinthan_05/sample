package com.galaxy.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class GalaxyServicesApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
	
		SpringApplication.run(GalaxyServicesApplication.class, args);
 
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		System.out.println("Server was UP !!!!---");
		return builder.sources(GalaxyServicesApplication.class);
	}
}

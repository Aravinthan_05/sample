package com.galaxy.services.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Customer {

    @Id
    @GeneratedValue
    private int id;
    
    @Column(name="Cust_code")
    private String custCode;
 



	private String ftth_no;
    private String first_name;
    private String last_name;
    private String address;
    private String pincode;
    private String city;
    private String contact_no;
    private String emailid;
    private String form_filling;
    private String cabelling_date;
    private String cable_used;
    private String splicing_date;
    private String device_ins_date;
    private String mac_addr;
    private String device_srno;
    
	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
    
    public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String password;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   

    public String getFtth_no() {
        return ftth_no;
    }

    public void setFtth_no(String ftth_no) {
        this.ftth_no = ftth_no;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public String getForm_filling() {
        return form_filling;
    }

    public void setForm_filling(String form_filling) {
        this.form_filling = form_filling;
    }

    public String getCabelling_date() {
        return cabelling_date;
    }

    public void setCabelling_date(String cabelling_date) {
        this.cabelling_date = cabelling_date;
    }

    public String getCable_used() {
        return cable_used;
    }

    public void setCable_used(String cable_used) {
        this.cable_used = cable_used;
    }

    public String getSplicing_date() {
        return splicing_date;
    }

    public void setSplicing_date(String splicing_date) {
        this.splicing_date = splicing_date;
    }

    public String getDevice_ins_date() {
        return device_ins_date;
    }

    public void setDevice_ins_date(String device_ins_date) {
        this.device_ins_date = device_ins_date;
    }

    public String getMac_addr() {
        return mac_addr;
    }

    public void setMac_addr(String mac_addr) {
        this.mac_addr = mac_addr;
    }

    public String getDevice_srno() {
        return device_srno;
    }

    public void setDevice_srno(String device_srno) {
        this.device_srno = device_srno;
    }



}

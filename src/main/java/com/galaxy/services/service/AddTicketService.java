package com.galaxy.services.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.galaxy.services.entity.AddTicket;
import com.galaxy.services.entity.CategoryList;
import com.galaxy.services.repository.AddCategoryRepository;
import com.galaxy.services.repository.AddTicketRepository;

@Service
public class AddTicketService {

    @Autowired
    private AddTicketRepository repository;
    
    @Autowired
    private AddCategoryRepository repository1;

    public AddTicket saveTicket(AddTicket ticket){
//        System.out.println("aaaaaaaaaaaaaaaaaaa --- >>>>>>>>  "+repository.getMaxEmpId());
//        ticket.setCust_code("GIS"+padLeftZeros(repository.getMaxCustId(),5));
        return repository.save(ticket);
       /* System.out.println(employee.getEmp_code());
        repository.insertEmp(employee.getEmp_code(),employee.getFirst_name(),employee.getLast_name(),
                employee.getAddress(),employee.getPincode(),employee.getCity(),
                employee.getContact_no(),employee.getEmailid());*/
    }


    public List<AddTicket> getTickets(){
        return repository.findAll();
    }

    public AddTicket getTicketById(int id){
        return repository.findById(id).orElse(null);
    }

    public String deleteTicket(int id){
        repository.deleteById(id);
        return "Ticket removed !! "+id;
    }

    public List<CategoryList> getCategories(){
        return repository1.findAll();
    }

 
}

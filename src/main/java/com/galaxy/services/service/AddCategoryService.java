package com.galaxy.services.service;

import com.galaxy.services.entity.CategoryList;
import com.galaxy.services.repository.AddCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddCategoryService {

    @Autowired
    private AddCategoryRepository repository;

    public CategoryList saveCategory(CategoryList catg){
//        System.out.println("aaaaaaaaaaaaaaaaaaa --- >>>>>>>>  "+repository.getMaxEmpId());
//        ticket.setCust_code("GIS"+padLeftZeros(repository.getMaxCustId(),5));
        return repository.save(catg);
       /* System.out.println(employee.getEmp_code());
        repository.insertEmp(employee.getEmp_code(),employee.getFirst_name(),employee.getLast_name(),
                employee.getAddress(),employee.getPincode(),employee.getCity(),
                employee.getContact_no(),employee.getEmailid());*/
    }


    public List<CategoryList> getCategories(){
        return repository.findAll();
    }

    public CategoryList getCategoryById(int id){
        return repository.findById(id).orElse(null);
    }

    public String deleteCategory(int id){
        repository.deleteById(id);
        return "Ticket removed !! "+id;
    }


    
}

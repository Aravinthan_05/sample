package com.galaxy.services.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.galaxy.services.common.GalaxyUtil;
import com.galaxy.services.entity.Customer;
import com.galaxy.services.model.request.EmployeeLogin;
import com.galaxy.services.repository.CustomerRepository;


@Service
public class AuthenticationService {
	
	
	@Autowired
	private  CustomerRepository customerRepository;
	
	@Autowired
	private GalaxyUtil geGalaxyUtil;
	
	public Customer loginValidation(EmployeeLogin employeeLogin) throws Exception
	{
	try
	{
		System.out.println(employeeLogin.getUserId());
		
	Customer customer = customerRepository.getCustomerByCustCode(employeeLogin.getUserId());
	if(customer != null)
	{
		if(customer.getPassword().equalsIgnoreCase(geGalaxyUtil.getSha256(employeeLogin.getPassWord())))
		{
			return customer;
		}else
		{
			throw new Exception("your Password is Wrong !");	 
		}
			
	}else 
	{
	throw new Exception("User id not Found !");	
	}
	}catch (Exception e) {
		throw e;
	}
	}
	
	
	public Customer forgetPassword(EmployeeLogin employeeLogin) throws Exception
	{
	try
	{
	Customer customer = customerRepository.getCustomerByCustCode(employeeLogin.getUserId());
	if(customer != null)
	{
		if(customer.getPassword().equalsIgnoreCase(geGalaxyUtil.getSha256(employeeLogin.getPassWord())))
		{
			return customer;
		}else
		{
			throw new Exception("your Password is Wrong !");	 
		}
			
	}else 
	{
	throw new Exception("User id not Found !");	
	}
	}catch (Exception e) {
		throw e;
	}
	}
	

}

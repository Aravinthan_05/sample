package com.galaxy.services.service;

import com.galaxy.services.common.GalaxyUtil;
import com.galaxy.services.entity.Employee;
import com.galaxy.services.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository repository;
    
    @Autowired
    private GalaxyUtil galaxyUtil;

    /*public String getMaxId(){
        Session sess = null;
        String code = "";
        try {
            SessionFactory fact = new
            Configuration().configure().buildSessionFactory();
            sess = fact.openSession();
            String SQL_QUERY = "SELECT max(id) FROM Employee";
            Query query = sess.createQuery(SQL_QUERY);
            List list = query.list();
            System.out.println("Max " + list.get(0));
            code = list.get(0).toString();
            sess.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return code;
    }*/

   /* public Employee getMaxId(){
        return  repository.getMaxEmpId();
    }*/

    public Employee saveEmployee(Employee employee){
//        System.out.println("aaaaaaaaaaaaaaaaaaa --- >>>>>>>>  "+repository.getMaxEmpId());
        employee.setEmp_code("GIS"+galaxyUtil.padLeftZeros(repository.getMaxEmpId(),3));
        return repository.save(employee);
//        System.out.println( repository.save(employee));
//        return "success";
       /* System.out.println(employee.getEmp_code());
        repository.insertEmp(employee.getEmp_code(),employee.getFirst_name(),employee.getLast_name(),
                employee.getAddress(),employee.getPincode(),employee.getCity(),
                employee.getContact_no(),employee.getEmailid());*/
    }

    public List<Employee> saveEmployees(List<Employee> employees){
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees(){
        return repository.findAll();
    }

    public Employee getEmployeeById(int id){
        return repository.findById(id).orElse(null);
    }

    public Employee updateEmployee(Employee employee){
        Employee existingEmployee = repository.findById(employee.getId()).orElse(null);
        existingEmployee.setFirst_name(employee.getFirst_name());
        existingEmployee.setLast_name(employee.getLast_name());
        existingEmployee.setAddress(employee.getAddress());
        existingEmployee.setPincode(employee.getPincode());
        existingEmployee.setCity(employee.getCity());
        existingEmployee.setContact_no(employee.getContact_no());
        existingEmployee.setEmailid(employee.getEmailid());
        return repository.save(existingEmployee);
    }
    
//    public int updateEmployeeid(Employee employee) {
//    	Employee existingEmployee = repository.findById(employee.getId()).orElse(null);
//    	
//    	return repository.setStatusForEARAttachment(existingEmployee.getEmailid(), existingEmployee.getId());
//    }

    public String deleteEmployee(int id){
        repository.deleteById(id);
        return "employee removed !! "+id;
    }


    
}

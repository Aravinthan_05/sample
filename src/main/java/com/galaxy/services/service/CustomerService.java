package com.galaxy.services.service;

import com.galaxy.services.common.GalaxyUtil;
import com.galaxy.services.entity.Customer;
import com.galaxy.services.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository repository;
    
    @Autowired
    private GalaxyUtil galaxyUtil;

    public Customer saveCustomer(Customer customer){
    	try
    	{
    		
    	        customer.setCustCode("GISC"+galaxyUtil.padLeftZeros(repository.getMaxCustId(),5));
    	        customer.setPassword(galaxyUtil.getSha256(galaxyUtil.getDefaultPassword())); // 6 Digit First Default PAssword :
    	        return repository.save(customer);
    	}catch (Exception e) {
			// TODO: handle exception
		}
//       
       /* System.out.println(employee.getEmp_code());
        repository.insertEmp(employee.getEmp_code(),employee.getFirst_name(),employee.getLast_name(),
                employee.getAddress(),employee.getPincode(),employee.getCity(),
                employee.getContact_no(),employee.getEmailid());*/
		return customer;
    }

    public List<Customer> saveCustomers(List<Customer> customers){
        return repository.saveAll(customers);
    }

    public List<Customer> getCustomers(){
        return repository.findAll();
    }

    public Customer getCustomerById(int id){
        return repository.findById(id).orElse(null);
    }

    public Customer updateCustomer(Customer customer){
        Customer existingEmployee = repository.findById(customer.getId()).orElse(null);
        existingEmployee.setFirst_name(customer.getFirst_name());
        existingEmployee.setLast_name(customer.getLast_name());
        existingEmployee.setAddress(customer.getAddress());
        existingEmployee.setPincode(customer.getPincode());
        existingEmployee.setCity(customer.getCity());
        existingEmployee.setContact_no(customer.getContact_no());
        existingEmployee.setEmailid(customer.getEmailid());
        return repository.save(existingEmployee);
    }

    public String deleteCustomer(int id){
        repository.deleteById(id);
        return "Customer removed !! "+id;
    }


 
}

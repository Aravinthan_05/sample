package com.galaxy.services.repository;

import com.galaxy.services.entity.CategoryList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface AddCategoryRepository extends JpaRepository<CategoryList,Integer>, CrudRepository<CategoryList,Integer> {
//
//    @Query(value = "SELECT max(substr(c.Cust_code,5))+1  FROM Customer c")
//    String getMaxCustId();
}

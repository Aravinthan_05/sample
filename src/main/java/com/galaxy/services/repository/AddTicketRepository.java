package com.galaxy.services.repository;

import com.galaxy.services.entity.AddTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface AddTicketRepository extends JpaRepository<AddTicket,Integer>, CrudRepository<AddTicket,Integer> {
//
//    @Query(value = "SELECT max(substr(c.Cust_code,5))+1  FROM Customer c")
//    String getMaxCustId();
}

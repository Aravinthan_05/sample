package com.galaxy.services.repository;

import com.galaxy.services.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends JpaRepository<Customer,Integer>, CrudRepository<Customer,Integer> {

    @Query(value = "SELECT max(substr(c.custCode,5))+1  FROM Customer c")
    String getMaxCustId();
    
    public Customer getCustomerByCustCode(String Cust_code);
}

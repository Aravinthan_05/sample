package com.galaxy.services.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.galaxy.services.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee,Integer>, CrudRepository<Employee,Integer> {
//
    @Query(value = "SELECT max(substr((ee.Emp_code),4))+1 FROM Employee ee")
    String getMaxEmpId();
    
    @Modifying
    @Query("update Employee emp set emp.emailid = ?1 where emp.id = ?2")
    int setStatusForEARAttachment(String emailid, Integer id);
/*
    @Modifying
    @Query(
            value =
                    "insert into Employee (Emp_code, first_name, last_name, address,pincode,city,contact_no,emailid) " +
                            " values (:Emp_code, :first_name, :last_name, :address, :pincode, :city, :contact_no, :emailid)",
            nativeQuery = true)
    void insertEmp(@Param("Emp_code") String Emp_code, @Param("first_name") String first_name,
                    @Param("last_name") String last_name, @Param("address") String address,
                   @Param("pincode") String pincode, @Param("city") String city,
                   @Param("contact_no") String contact_no, @Param("emailid") String emailid);*/
}
